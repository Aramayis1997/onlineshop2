<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect; // note
use App\ProductModel;
use App\PhotoModel;
use App\ProductCartModel;
use App\WishlistModel;
use Session;
class ProductController extends Controller
{
    function AddProducts(){
        return view('AddProducts');
    }
    function AddNewProduct(Request $add){
        $AddNew = Validator::make($add->all(),
        [
            'name' => 'required|string',
            'count' => 'required|numeric|max:100',
            'price' => 'required|numeric',
            'description' => 'required|string',

        ]
    );
    if($AddNew->fails()){
        return Redirect::to('/AddProducts')->withErrors($AddNew)->withInput();
        
    }
    else{
        $info = ProductModel::where('user_ID' , Session::get('id'))->first();
        $info = new ProductModel();
        $info->Product_Name = $add->name;
        $info->Count = $add->count;
        $info->Price = $add->price;
        $info->Description = $add->description;
        $info->user_ID = Session::get('id');
        $info->save();
        if($add->hasfile('photo')){
            foreach($add->file("photo") as $img){
                $address = time().$img->getClientOriginalName();
                $img->move(public_path()."/ProductPhoto/" , $address);
                $nkar = new PhotoModel();
                $nkar -> Photo = $address;
                $nkar -> Product_id = $info->id;
                $nkar ->save();
            }
        }
      }
      return view('AddProducts');
      
    }
    function delete(Request $r){
        ProductModel::where('ID' , $r->deleteval)->delete();
      }
    function item($id){
        $prodinfo = ProductModel::where('ID' ,'=' , $id)->first();
        return view('product-item')->with('prodinfo' , $prodinfo);
    }


    function showproducts(Request $r){
        $info = ProductModel::where('ID' , $r->prod_ID)->first();
        return view('showproducts')->with('info', $info);
    }
    function editprod(Request $update){
        $newupdate = Validator::make($update->all(),
        [
            'prod_name' => 'required|string',
            'prod_count' => 'required|numeric|max:100',
            'prod_price' => 'required|numeric',
            'prod_info' => 'required|string',
        ],
        [
            'required' => 'This field is required',
        ]
    );
    if($newupdate->fails()){
        return Redirect::to('/showproducts')->withErrors($newupdate)->withInput();
        
    }
    else{
        ProductModel::where('ID' , Session::get('id'))->update(
            [
                'Product_Name' => $info->prod_name,
                
            ]
                
            );
         }
        return Redirect::to('/showproducts');
    }
    function AddToCart($id){
        $cart = new ProductCartModel();
        $cart->Product_ID = $id;
        $cart->User_ID = Session::get('id');
        $cart->Count = 1;
        $cart->save();
        return Redirect::to('/profile');
    }
    function AddtoWishlist($id){
        $wishlist  = new WishlistModel();
        $wishlist->Product_ID = $id;
        $wishlist->User_ID = Session::get('id');
        $wishlist->save();
        return Redirect::to('/profile')->with('wishlist' , $wishlist);
    }
}
//stripe.com vhcrayin hamakragi hamar
