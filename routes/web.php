<?php


Route::get('/', function () {
    return view('welcome');
});


Route::get('/signup' , 'UserController@signup');

Route::post('/register' , 'UserController@register'); //1parametr - url , 2 parametr - function

Route::get('/login' , 'UserController@login');

Route::post('/loginprof' , 'UserController@loginprof');

Route::get('/logout' , 'UserController@logout');

Route::get('/profile' , 'UserController@profile')->middleware("checkLogin");  //stugum enq naxapes vor middleware ka

Route::get('/editdata' , 'UserController@editdata')->middleware("checkLogin");

Route::post('/edit' , 'UserController@edit')->middleware("checkLogin");

Route::get('/AddProducts' , 'ProductController@AddProducts')->middleware("checkLogin");

Route::post('/AddNewProduct' , 'ProductController@AddNewProduct')->middleware("checkLogin");

Route::get('/product/item/{id}' , 'ProductController@item')->middleware("checkLogin");

Route::post('/editprod', 'ProductController@editprod')->middleware("checkLogin");

Route::get('/showproducts' ,'ProductController@showproducts')->middleware("checkLogin");

Route::post('/deleteprod' , 'ProductController@delete')->middleware("checkLogin");

Route::get('/AddToCart/{id}', 'ProductController@AddToCart');

Route::get('/AddtoWishlist/{id}' , 'ProductController@AddtoWishlist');

Route::get('account/verify/{href}/{id}', 'UserController@Verify');

Route::get('stripe', 'StripePaymentController@stripe');

Route::post('stripe', 'StripePaymentController@stripePost')->name('stripe.post');



