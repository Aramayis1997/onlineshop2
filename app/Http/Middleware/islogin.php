<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Illuminate\Support\Facades\Redirect;

class islogin

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::has('id')){

            return $next($request)
            ->header('Cache-Control', 'no-cache,no-store,max-age=0,must-revalidate');

        }
        else{
            return Redirect::to('/login')->with('message', 'You are logged out');
            // ete return view a apa tpum enq $message ete redirect::to apa pahvum e sessioni mej
        }
    }
}

