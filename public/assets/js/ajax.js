$(document).ready(function(){

    const token = $("#token").val();

    $('.deleteproduct').on('click' , function(){
        var deleteval = $(this).val()
        var thisdiv = $(this)
        $.ajax({
            url: '/deleteprod',
            type:'post',
            data:{
                deleteval,
                "_token":token
            },
            success:function(r){
                thisdiv.parent().parent().parent().remove()
            }
        })
    })
    
})