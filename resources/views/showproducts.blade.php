    <!-- @extends('layouts.layout'); -->

<!-- @section('content') -->

<!-- @endsection -->


<!-- @section('styles') -->
<!-- <link href></link> -->
<!-- anahatak stylery -->
<!-- <title>Products</title> -->

<!-- @endsection -->

<form action="{{url('/editprod')}}" method='post' enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="text" name="prod_name" value='{{$info->Product_Name}}'>
    <input type="text" name="prod_count" value='{{$info->Count}}'  >
    <input type="text" name="prod_price" value='{{$info->Price}}'>
    <br><br>
    <textarea style='resize:none' name="description" value='@if(!$errors->first("description")){{old("description")}}@endif' placeholder="{{$errors->first('description')}}"></textarea>
    <br><br>
    Photo:<input type="file" name="photo[]" multiple>
    <br><br>
    <button>Save Changes</button>
    </form>