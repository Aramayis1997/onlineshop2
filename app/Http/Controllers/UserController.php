<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Illuminate\Support\Facades\Redirect; // note
use App\UserModel;
use Session;
use App\ProductModel;
use App\ProductCartModel;
use App\WishlistModel;
use Hash;
use Mail;
class UserController extends Controller
{
    // function ok(){
    //     print 'ok';
    // }
    // function das1(){
    //     $people = [
    //         ['name' => "Gayane" , "age" => 23],
    //         ['name' => "Armen" , "age" => 25],
    //         ['name' => "Artur" , "age" => 30],
    //         ['name' => "Vahe" , "age" => 11],

    //     ];
    //     $user = ["Ani" , "Armen"];
    //     return view ('das1')->with("name", "Ani")->with("anun", $user)->with('mardik' , $people); // name - popoxakan , Ani- arjeq, with-mi bani het miasin
    // }

    function signup(){
        return view ('signup');
    }
    function register(Request $r){

        $validation = Validator::make($r->all(), 
    
            [
                'name' => 'required|string',
                'surname' => 'required|string',
                'age' => 'required|numeric|min:2',
                'email' => 'required|email',
                'password' => 'required',
                'confirm' => 'required|same:password',
            ],
            [
                'required' =>'Empty Field',
                // 'name.required' => 'Name Field should not be empty',
            ]
    
        );
        if($validation->fails()){
            return Redirect::to('/signup')->withErrors($validation)->withInput();
            
        }
        else{
            $user = new UserModel();
            $user->Name = $r->name;
            $user->Surname  = $r->surname;
            $user->email = $r->email;
            $user->age = $r->age;
            $user->password =  Hash::make($r->password);
            $user->save();
            $userinfo =[
                'name' =>$user->Name,
                'id' => $user->id,
                'href' =>  md5($user->id.$user->email),
            ];
            Mail::send('mail' ,$userinfo,
            function ($m) use ($user){
                $m->from('yeghiazaryanaramayis@gmail.com' , 'Shop admin');
                $m->to($user->email, "$user->Name $user->Surname")->subject('SPAM');

            }
        ); 
            //1 paramentr blade anun 2parametr inch petq a uxarkenq, 3 parament function
      
            return Redirect::to('/login');
        }
    
    
    }
    function Verify($href, $id){

        $user = UserModel::where('ID', $id)->first();
        if($user){
            if(md5($user->ID.$user->email) == $href){
                UserModel::where('ID', $id)->update(
                    [ 'Active' => 1]
                );
                return Redirect::to('/login');
            }
        }
       
    }
    function login(){
        return view('login');
    }
    // postov ekac grum enq parametr request
    function loginprof(Request $r){
        $validation = Validator::make($r->all(), 
        [
            'email' => 'required|email',
            'password' => 'required',
        ],
        [
            'required' =>'This field is required',
        ]
    );
        $user = UserModel::where("email" , "=" , $r->email)->first();
        $validation->after(function($validation) use ($user,$r){
            if(!$user){
                $validation->errors()->add('email' , 'Chka Email');
            }
            if(!Hash::check($r->password, $user['password'])){
                $validation->errors()->add('password', "Sxal Password");
            }
            if($user->Active == 0){
                $validation->errors()->add('email' , 'Verify Email');
            }
        });
            if($validation->fails()){
                    return Redirect::to('/login')->withErrors($validation)->withInput();  
         }
            else{
                Session::put('id',$user['ID']);
                    return Redirect::to("/profile");    
        }
    }
    function profile(){
        $user = UserModel::where('ID',Session::get('id'))->first();
        $info1 = ProductModel::where('user_ID',Session::get('id'))->get();
        $show = ProductModel::all();
        $cart =  ProductCartModel::where('User_ID' , Session::get('id'))->get();
        $wishlist = WishlistModel::where('User_ID' , Session::get('id'))->get();
        return view('profile')->with('user', $user)->with('info', $info1)->with('show' , $show)->with('cart',  $cart)->with('wishlist' , $wishlist);
    }
    function logout(){
        Session::flush();
        return Redirect::to('/login'); 
    }
    function editdata(){
        $user = UserModel::where('ID' , Session::get('id'))->first();
        return view('edit')->with('user', $user);
    }
    function edit(Request $r){
        $validation = Validator::make($r->all(), 
    
        [
            'name' => 'required|string',
            'surname' => 'required|string',
            'age' => 'required|numeric|min:2',
            'email' => 'required|email',
            'password' => 'required',
        ],
        [
            'required' =>'This field is required',
        ]

    );
    $user = UserModel::where('ID' , Session::get('id'))->first();
    $email = UserModel::where('email',$r->email)->where('email','!=',$user->email )->first();
   
        $validation->after(function($validation) use ($email){
            if($email){
                $validation->errors()->add('email' , 'ka Email');
            }
        });
            if($validation->fails()){
                return Redirect::to('/editdata')->withErrors($validation)->withInput();  
         }
         else{
          UserModel::where('ID' , Session::get('id'))->update(
                [
                    'Name' => $r->name,
                    'Surname' => $r->surname,
                    'email' => $r->email,
                    'age' => $r->age,
                    'password' => Hash::make($r->password),
                ]
                
            );
         }
            
             return Redirect::to('/profile');
    }

    
    
}
// config clear cache