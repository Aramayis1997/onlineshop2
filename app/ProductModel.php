<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
// use App\PhotoModel;
class ProductModel extends Model
{
    public $table = 'products';
    public $timestamps = false;

    public function photos(){
        
        return $this->hasMany(
            'App\PhotoModel', 'Product_id' , 'ID'
        );

    }
    public function saller(){
        
        return $this->belongsTo(
            'App\UserModel' , 'user_ID' , 'ID' 
        );
        // belongsTo one to many kapy

    }
}
