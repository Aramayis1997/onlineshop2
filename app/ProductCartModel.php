<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCartModel extends Model
{
    public $table = 'cart';
    public $timestamps = false;

    function Cart(){
        return $this->belongsTo(
            'App\ProductModel' , 'Product_ID' , 'ID'
    
        );
    }
    function Users(){
        return $this->belongsTo(
            'App\User' , 'User_ID' , 'ID'
        );
    }

}
