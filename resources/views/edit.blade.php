<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <form action="{{url('/edit')}}" method='post'>
            {{csrf_field()}}
            {{$errors->first('name')}}
            <input type="text" name="name" value='{{$user->Name}}'>
            <br><br>
            {{$errors->first('surname')}}
            <input type="text" name="surname" value='{{$user->Surname}}'>
            <br><br>
            {{$errors->first('email')}}
            <input type="text" name="email" value='{{$user->email}}' >
            <br><br>
            {{$errors->first('age')}}
            <input type="text" name="age" value='{{$user->age}}'>
            <br><br>
            {{$errors->first('password')}}
            <input  name="password" placeholder='New Password' type='password'>
            <br><br>
            <button>Edit Changes</button>
    </form>

</body>
</html>