<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WishlistModel extends Model
{
    public $table = 'wishlist';
    public $timestamps = false;

    
    function Users(){
        return $this->belongsTo(
            'App\UserModel' , 'User_ID' , 'ID'
        );
        }
        function Products(){
            return $this->belongsTo(
                'App\ProductModel' , 'Product_ID' , 'ID'
            );
        }
    }
