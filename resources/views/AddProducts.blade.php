<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>AddNewProducts</title>
</head>
<body>
    <form action="{{url('/AddNewProduct')}}" method='post' enctype="multipart/form-data">
    {{csrf_field()}}
    <input type="text" name="name" value='@if(!$errors->first("name")){{old("name")}}@endif' placeholder="{{$errors->first('name')}}">
    <input type="text" name="count" value='@if(!$errors->first("count")){{old("count")}}@endif' placeholder="{{$errors->first('count')}}">
    <input type="text" name="price" value='@if(!$errors->first("price")){{old("price")}}@endif' placeholder="{{$errors->first('price')}}">
    <br><br>
    <textarea style='resize:none' name="description" value='@if(!$errors->first("description")){{old("description")}}@endif' placeholder="{{$errors->first('description')}}"></textarea>
    <br><br>
    Photo:<input type="file" name="photo[]" multiple>
    <br><br>
    <button>Add Product</button>
    </form>
</body>
</html>

<!-- middleware - stugum enq ardyoq sessiayum id ka te che -->