<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhotoModel extends Model
{
    public $table = 'photos';
    public $timestamps = false;
}
